package com.ksy.newsscrap.controller;

import com.ksy.newsscrap.model.NewsItem;
import com.ksy.newsscrap.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<NewsItem> getHtml() throws IOException {
        List<NewsItem> result = scrapService.run(); // run에서 result를 받아서
        return result; // 사람에게 보여줌
    }
}
