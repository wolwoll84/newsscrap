package com.ksy.newsscrap.service;

import com.ksy.newsscrap.model.NewsItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException {
        String url = "http://www.astronomer.rocks/"; // 접속할 주소
        Connection connection = Jsoup.connect(url); // Jsoup 추가 기능을 이용해서 url = 윗주소로 접속하라 / 접속한 상태값이 connection에 담긴다

        Document document = connection.get(); // 접속한 상태에서 html 소스를 긁어와서 문서화

        return document; // run에게 document를 준다
    }

    private List<Element> parseHtml(Document document) { // document를 받아서 Element 모양의 List를 주기로 했다
        Elements elements = document.getElementsByClass("auto-article"); // 문서의 클래스에서 auto-article이 들어간 것들을 찾는다 (이유 : li가 생각보다 많이 있음. 기사의 li만 찾고 싶어서 규칙 찾아봄. 기사는 클래스에 auto-article이 있음. 1차 거름망)

        List<Element> tempResult = new LinkedList<>(); // 리스트 연결

        for (Element item : elements){ // auto-article 중 Element를 하나씩 넣어서
            Elements lis = item.getElementsByTag("li"); // li 태그가 들어간 것들만 분리. 복수여서 Elements가 됨
            for (Element item2 : lis) { // li도 하나하나 분리해서
                tempResult.add(item2); // tempResult에 넣기
            }
        }

        return tempResult; // run에게 tempResult을 돌려주다
    }

    private List<NewsItem> makeResult(List<Element> list) { // 리스트 받아서 List<NewsItem>로 만들어줌
        List<NewsItem> result = new LinkedList<>();

        for (Element item : list) {
            Elements checkContents = item.getElementsByClass("flow-hidden"); // 기사가 아닌 자료(flow-hidden) 제거해야함. 제거할 내용에 해당하는 규칙을 찾아야 한다... flow-hidden를 찾기
            if (checkContents.size() == 0) { // flow-hidden이 없다면 { } 실행하기
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB"); // 안 보이는 광고도 걸러야함. 정상적인 기사에는 auto-fontB가 포함되어 있는데, 광고에는 없음. auto-fontB를 찾기
                if (checkHiddenBanner.size() > 0) { // auto-fontB가 있으면 { } 실행하기
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text(); // li 소스코드를 보면 제목은 auto-titles 안에 있음. get(0) = 첫번째거를 가져와라. 그 안에서 strong 태그 붙은 것 중 첫번째를 가져와라. 텍스트를 추출하라.
                    String content = item.getElementsByClass("auto-fontB").get(0).text(); // 클래스에 auto-fontB가 포함된 첫번째 것을 가져와라. 텍스트 추출하라.

                    NewsItem addItem = new NewsItem(); // NewsItem 모델 사용해서 그릇 만들기. 2칸짜리.
                    addItem.setTitle(title); // 그릇 첫번째 칸에 세팅(내용물)
                    addItem.setContent(content); // 그릇 두번째 칸에 세팅(내용물)

                    result.add(addItem); // 완성된 그릇을 저장
                }
            }
        }

        return result; // 결과를 run에게 돌려주다
    }

    public List<NewsItem> run() throws IOException { // 런은 List에 NewsItem이란 걸 준다
        Document document = getFullHtml(); // getFullHtml에게 document 만들라고 시킴. 받아와서 run이 가진 박스에 넣을 거임
        List<Element> elements = parseHtml(document); // document를 받았으니 parseHtml에게 파싱하라고 시킴
        List<NewsItem> result = makeResult(elements); // 파싱된 리스트를 받으면 elements라는 박스에 잘 넣어두고, makeResult에게 결과를 가공하라고 시킴(제목/내용 분리)

        return result; // makeResult에서 result를 받아서 컨트롤러에게 돌려주다
    }
}
