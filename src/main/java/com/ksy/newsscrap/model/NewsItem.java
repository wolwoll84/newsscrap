package com.ksy.newsscrap.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsItem {
    private String title;
    private String content;
}
