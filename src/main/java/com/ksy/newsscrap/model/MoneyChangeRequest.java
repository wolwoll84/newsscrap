package com.ksy.newsscrap.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyChangeRequest {
    private Integer money;
}
